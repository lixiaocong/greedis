cmake_minimum_required(VERSION 3.10)
set(CMAKE_CXX_STANDARD 17)

project(greedis_server)

include_directories(${CMAKE_SOURCE_DIR}/include/libevent)
link_directories(${CMAKE_SOURCE_DIR}/lib/${CMAKE_SYSTEM_NAME}/libevent)

add_executable(greedis_server main.cpp)

target_link_libraries(greedis_server greedis)

add_subdirectory(src)
add_subdirectory(test)