#include <iostream>
#include "client.h"

#define MAX_READ_BYTE 1024*16 // 16KB

using namespace std;

const int NOT_DEFINED = -1;

const int ERR_OK = 0;
const int ERR_NOT_FINISHED = 1;
const int ERR_WRONG_FORMAT = 2;

greedis::Client::Client() {
    m_commands_number = NOT_DEFINED;
    m_current_command_idx = 0;
    m_current_command_size = NOT_DEFINED;
}

greedis::Client::~Client() = default;

void greedis::Client::HandleReadEvent(struct bufferevent *bev) {
    char line[MAX_READ_BYTE];
    int byte_read;
    while (byte_read = bufferevent_read(bev, line, MAX_READ_BYTE), byte_read > 0) {
        m_input_buffer.append(line, byte_read);
    }
    auto ret = ReadRequestFromInputBuffer();
    if (ret == ERR_OK) {
        auto num_cmd = ProcessCommand();
        char buffer[1000];
        sprintf(buffer, "%d cmd args processed", num_cmd);
        AddResponseToOutputBuffer(buffer);
    } else if (ret == ERR_NOT_FINISHED) {
        return;
    } else {
        AddResponseToOutputBuffer("command error");
    }
    HandleWriteEvent(bev);
}

void greedis::Client::HandleWriteEvent(struct bufferevent *bev) {
    cout << "handle write event --------------------------------" << endl;
    auto err = bufferevent_write(bev, m_output_buffer.c_str(), m_output_buffer.size());
    if (err == ERR_OK) {
        cout << "write done:" << m_output_buffer << endl;
        m_output_buffer.clear();
        return;
    }
    cout << "write error" << endl;
}

int greedis::Client::ReadRequestFromInputBuffer() {
    if (m_commands_number == NOT_DEFINED) {
        auto ret = ReadInputCommendsNumber();
        if (ret != ERR_OK) {
            return ret;
        }
        cout << "commands number = " << m_commands_number << endl;
    }
    while (m_current_command_idx < m_commands_number) {
        if (m_current_command_size == NOT_DEFINED) {
            auto ret = ReadInputCommendLength();
            if (ret != ERR_OK) {
                return ret;
            }
            cout << "command len = " << m_current_command_size << endl;
        }
        auto ret = ReadInputCommendValue();
        if (ret != ERR_OK) {
            return ret;
        }
        m_current_command_size = NOT_DEFINED;
        m_current_command_idx++;
    }
    m_commands_number = NOT_DEFINED;
    m_current_command_idx = 0;
    return ERR_OK;
}

int greedis::Client::ReadInputCommendsNumber() {
    auto pos = m_input_buffer.find_first_of("\r\n");
    if (pos == string::npos) {
        return ERR_NOT_FINISHED;
    }
    if (m_input_buffer[0] != '*') {
        return ERR_WRONG_FORMAT;
    }
    auto commend_number_str = m_input_buffer.substr(1, pos - 1);
    auto command_number = stoi(commend_number_str);
    m_commands_number = command_number;
    m_input_buffer = m_input_buffer.substr(pos + 2);
    return ERR_OK;
}

int greedis::Client::ReadInputCommendLength() {
    auto pos = m_input_buffer.find_first_of("\r\n");
    if (pos == string::npos) {
        return -1;
    }
    if (m_input_buffer[0] != '$') {
        return -1;
    }
    auto commend_len_str = m_input_buffer.substr(1, pos - 1);
    auto command_len = stoi(commend_len_str);
    m_current_command_size = command_len;
    m_input_buffer = m_input_buffer.substr(pos + 2);
    return 0;
}

int greedis::Client::ReadInputCommendValue() {
    auto pos = m_input_buffer.find_first_of("\r\n");
    if (pos == string::npos) {
        return -1;
    }
    auto command_arg = m_input_buffer.substr(0, pos);
    if (command_arg.length() != m_current_command_size) {
        cout << "cmd len diff" << endl;
        return -1;
    }
    m_input_args.push(command_arg);
    cout << "command value = " << command_arg << endl;
    m_input_buffer = m_input_buffer.substr(pos + 2);
    return 0;
}

int greedis::Client::ProcessCommand() {
    auto num_cmd = m_input_args.size();
    m_input_args.front();
    while (!m_input_args.empty()) {
        auto arg = m_input_args.front();
        m_input_args.pop();
        cout << "------ " << arg << endl;
    }
    return num_cmd;
}

int greedis::Client::AddResponseToOutputBuffer(const string &msg) {
    char response[1000];
    sprintf(response, "*1\r\n$%zu\r\n%s\r\n", msg.length(), msg.c_str());
    m_output_buffer.append(response, strlen(response));
    cout << "write out put buffer done" << endl;
    return 0;
}

