//
// Created by lixiaocong on 2020/5/24.
//

#ifndef GREEDIS_EVENT_HANDLER_H
#define GREEDIS_EVENT_HANDLER_H

#include <event2/event.h>

namespace greedis {
    class AcceptEventHandler {
    public:
        virtual void HandleAcceptEvent(evutil_socket_t listen_fd) = 0;
    };

    class ReadEventHandler {
    public:
        virtual void HandleReadEvent(struct bufferevent *bev) = 0;
    };

    class WriteEventHandler {
    public:
        virtual void HandleWriteEvent(struct bufferevent *bev) = 0;
    };

    class ErrorEventHandler {
    public:
        virtual void HandleErrorEvent(struct bufferevent *bev, short event) = 0;
    };
}

#endif //GREEDIS_EVENT_HANDLER_H
