//
//

#ifndef GREEDIS_CONST_H
#define GREEDIS_CONST_H

namespace greedis {
    typedef unsigned int err_num_t;
    const err_num_t ERR_OK = 0;
}

#endif //GREEDIS_CONST_H
