//
//

#include "engine.h"

greedis::Engine::Engine() {
}

greedis::Engine::~Engine() {

}

greedis::err_num_t greedis::Engine::Set(string key, string value) {
    m_dict[key] = value;
    return ERR_OK;
}

string greedis::Engine::Get(string key) {
    auto value = m_dict.find(key);
    if (value != m_dict.end()) {
        return value->second;
    }
    return NULL;
}
