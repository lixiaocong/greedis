#ifndef GREEDIS_SERVER_H
#define GREEDIS_SERVER_H

#include <string>
#include <list>
#include "event_loop.h"
#include "client.h"

using namespace std;

#define LISTEN_BACKLOG 32

namespace greedis {

    class Server : AcceptEventHandler, ErrorEventHandler {
    private:
        int m_port;
        EventLoop *m_event_loop;
        list<Client *> m_client;

        void HandleAcceptEvent(int listener) override;

        void HandleErrorEvent(struct bufferevent *bev, short i) override;

    public:
        Server();

        ~Server();

        int Start();
    };

} // namespace greedis
#endif //GREEDIS_SERVER_H
