#ifndef GREEDIS_CLIENT_H
#define GREEDIS_CLIENT_H

#include <event2/event.h>
#include <string>
#include <queue>
#include "event_loop.h"

using namespace std;

namespace greedis {
    class Client : public ReadEventHandler, public WriteEventHandler {
    private:

        string m_input_buffer;
        string m_output_buffer;
        int m_commands_number;
        int m_current_command_idx;
        int m_current_command_size;
        queue<string> m_input_args;

        void HandleReadEvent(struct bufferevent *bev) override;

        void HandleWriteEvent(struct bufferevent *bev) override;

        int ReadRequestFromInputBuffer();

        int ReadInputCommendsNumber();

        int ReadInputCommendLength();

        int ReadInputCommendValue();

        int ProcessCommand();

        int AddResponseToOutputBuffer(const string &msg);

    public:
        Client();

        ~Client();
    };
};
#endif //GREEDIS_CLIENT_H
